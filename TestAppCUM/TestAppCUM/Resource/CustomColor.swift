//
//  CustomColor.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 25/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import UIKit

enum CustomColor {
    
    /// - #5e6b7b
    /// - rgb(94 107 123)
    static let currencyAccent = #colorLiteral(red: 0.368627450980392,green: 0.419607843137255, blue: 0.482352941176471, alpha: 1)
    
    /// - #69ca72
    /// - rgb(105 202 114)
    static let green = #colorLiteral(red: 0.411764705882353,green: 0.792156862745098, blue: 0.447058823529412, alpha: 1)
    
    /// - #fcfcfc
    /// - rgb(252 252 252)
    static let lightSeparator = #colorLiteral(red: 0.988235294117647,green: 0.988235294117647, blue: 0.988235294117647, alpha: 1)
    
    /// - #f44336
    /// - rgb(244 67 54)
    static let red = #colorLiteral(red: 0.956862745098039,green: 0.262745098039216, blue: 0.211764705882353, alpha: 1)
    
    /// - #e0e4ea
    /// - rgb(224 228 234)
    static let separator = #colorLiteral(red: 0.87843137254902,green: 0.894117647058824, blue: 0.917647058823529, alpha: 1)
    
    /// - #7f7f7f
    /// - rgb(127 127 127)
    static let tagTitleDefault = #colorLiteral(red: 0.498039215686275,green: 0.498039215686275, blue: 0.498039215686275, alpha: 1)
    
    /// - #FF6600
    /// - rgb(255 102 0)
    static let yellow = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
}
