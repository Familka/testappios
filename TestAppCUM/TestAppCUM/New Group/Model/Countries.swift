//  Created by Фамил Гаджиев on 20/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import Foundation

/// Города для главной страницы
struct Countries: Decodable {
    
    /// Название города
    let name: String
    
    /// Информация о количестве жителей
    let population: Int
}


extension Countries {
    private enum CodingKeys: String, CodingKey {
        case name, population
    }
}
