//
//  ServiceLayer.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 26/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import Moya

/// Сервисный слой
///
/// getAllCountry: - Получить весь список стран и распарсить
/// getCountry: - Получить единичный объект
struct ServiceLayer {
//    static let provider = MoyaProvider<PageRequest>(plugins: [NetworkLoggerPlugin(verbose: true)])
    static let provider = MoyaProvider<PageRequest>()
    
    static func getAllCountry(completion: @escaping ([Countries]) -> (),
                              progress: @escaping ProgressBlock) {
        
        provider.request(.all,
                         callbackQueue: DispatchQueue.main,
                         progress: progress) { result in
                    progress(ProgressResponse())
                            print(ProgressResponse().progress)
            switch result {
            case .success(let response):
                do {
                    let results = try JSONDecoder().decode([Countries].self, from: response.data)
                    completion(results)
                } catch {
                    print("Parsing Error")
                }
            case .failure(_):
                print("Network Error")
            }
        }
    }
    
    static func getCountry(name country: String,
                           completion: @escaping([Country]) -> (),
                           progress: @escaping ProgressBlock) {
       
        provider.request(.search(country),
                         callbackQueue: DispatchQueue.main,
                         progress: progress) { result in
            switch result {
            case .success(let response):
                do {
                    let results = try JSONDecoder().decode([Country].self, from: response.data)
                    completion(results)
                } catch {
                    print("Parsing Error")
                }
            case .failure(_):
                print("Network Error")
            }
        }
    }
}
