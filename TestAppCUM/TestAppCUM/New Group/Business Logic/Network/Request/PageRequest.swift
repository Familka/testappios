//
//  Request.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 24/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import Foundation

public enum PageRequest {
    case all
    case search(String)
    
    public var baseURL: URL {
        return URL(string: "https://restcountries.eu/rest/v2")!
    }
}
