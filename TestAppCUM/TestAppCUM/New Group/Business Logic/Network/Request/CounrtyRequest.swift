//
//  CounrtyRequest.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 25/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import Moya

extension PageRequest: TargetType {
    
    public var path: String {
        switch self {
        case .all: return "/all"
        case .search(let search): return "/name/\(search)"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .all: return .get
        case .search: return .get
        }
    }
    
    public var task: Task {
        switch self {
        case .all: return .requestPlain
        case .search(_): return .requestPlain
        }
    }
    
    public var sampleData: Data { return Data() }
    
    public var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
}
