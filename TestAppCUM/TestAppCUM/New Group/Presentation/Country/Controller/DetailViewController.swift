//
//  DetailViewController.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 26/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

final class DetailViewController: UIViewController {
    let disposeBag = DisposeBag()

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var sity: UILabel!
    @IBOutlet weak var population: UILabel!
    @IBOutlet weak var borders: UILabel!
    @IBOutlet weak var currencies: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    let nameCountry: String?
    
    private var contryObservable = Variable<[Country]>([])

    init(nameCountry: String){
        self.nameCountry = nameCountry
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBarButtons()
        fetchCountry()
    }
    
    func fetchCountry() {
        self.progressView.setProgress(0, animated: true)
        guard let country = nameCountry else { return }
        ServiceLayer.getCountry(
            name: country,
            completion: {
                [weak self] result in
                
                self?.contryObservable.value = result
                self?.d()
                
        }) { progress in
            print(progress.progress)
            self.progressView.setProgress(Float(progress.progress), animated: true)
            sleep(1)
            self.progressView.alpha = 0
        }
    }

    func d() {
        contryObservable.asObservable()
            .subscribe(onNext: { [weak self] country in
                let model = country[0]
                self?.configure(model)
            }).disposed(by: disposeBag)
    }
}

extension DetailViewController {
    private func setupBarButtons() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Закрыть", style: .plain, target: self, action: #selector(dismiss(_:)))
    }
    
    @objc func dismiss(_ sender: Any?) {
        dismiss(animated: true, completion: nil)
    }
}

extension DetailViewController {
    func configure(_ country: Country) {
        name.text = country.name
        sity.text = country.capital
        population.text = String(country.population)
        borders.text = country.borders.joined(separator: ",")
        currencies.text = country.currencies
            .map({ $0.name })
            .joined(separator: ",")
    }
}
