//
//  NibRepresentable.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 26/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import UIKit

/// Тип способный быть созданым из `UINib`.
///
/// Используйте вместе с ячейками имеющими *.xib файлы.
protocol NibRepresentable: NSObjectProtocol {
    
    /// `UINib` содержащий класс.
    static var nib: UINib { get }
    
    /// Названия *.xib файла.
    static var nibName: String { get }
}

extension NibRepresentable {
    
    /// `UINib` содержащий класс.
    static var nib: UINib {
        return UINib(nibName: nibName, bundle: nil)
    }
    
    /// Названия *.xib файла.
    static var nibName: String {
        return String(describing: self)
    }
}
