//
//  TableViewExtension.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 26/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import UIKit

protocol NibLoadableView: class { }

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}

protocol ReusableView: class {}

extension ReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: NibLoadableView, ReusableView {}

extension UITableView {
    func register<T: UITableViewCell>(_: T.Type) {
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
}

extension UICollectionViewCell: NibLoadableView, ReusableView {}

extension UICollectionView {
    func register<T: UICollectionViewCell>(_ : T.Type) {
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
}
