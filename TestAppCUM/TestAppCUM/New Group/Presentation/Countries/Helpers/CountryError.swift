//
//  CountryError.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 26/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import Foundation

/// Ошибки
///
/// - notFound: Не найдена страница
enum CountryError: Error {
    case notFound
}

extension CountryError: LocalizedError {
    
    var errorDescription: String? {
        switch self {
        case .notFound: return "Ошибка"
        }
    }
    
    var recoverySuggestion: String? {
        switch self {
        case .notFound: return "Такой город не найден"
        }
    }
}
