//
//  CountriesTableViewController.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 26/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import UIKit

protocol CountriesTableControllerDelegate: class {
    func selectCountryViewController(_ controller: CountriesTableController, didSelect country: String)
}

final class CountriesTableController: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var models: [Countries] = []
    weak var delegate: CountriesTableControllerDelegate?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(CountriesTableViewCell.self)
        let cell = tableView.dequeueReusableCell(
            withIdentifier: CountriesTableViewCell.reuseIdentifier,
            for: indexPath) as! CountriesTableViewCell
        cell.configure(models[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nameCountry = models[indexPath.row].name
        delegate?.selectCountryViewController(self, didSelect: nameCountry)
    }
    
   
    // MARK: -  Animation
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.1) {
            if let cell = tableView.cellForRow(at: indexPath) as? CountriesTableViewCell {
                cell.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = tableView.cellForRow(at: indexPath) as? CountriesTableViewCell {
                cell.transform = .identity
            }
        }
    }
}
