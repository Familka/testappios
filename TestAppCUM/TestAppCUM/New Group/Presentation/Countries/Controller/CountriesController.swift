//
//  CityViewController.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 22/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

final class CountriesController: UIViewController {
    private let disposeBag = DisposeBag()
   
    private let tableController: CountriesTableController
    private weak var countriesTableView: CountriesTableView?
    
    init( tableController: CountriesTableController = .init()) {
        self.tableController = tableController
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super .viewDidLoad()
        title = "Страны"
        
        countriesTableView?.refreshControll.addTarget(self,
                                                      action: #selector(handleRefresh(_:)),
                                                      for: .valueChanged)
        countriesTableView?.progressBar.alpha = 1
        
        ServiceLayer.getAllCountry(completion: { [weak self] result in
            self?.tableController.models = result
            sleep(1) // Задержка сделана, что бы увидеть прогресс бар(получился прогресс бар очень маленьким, но рабочим)
            self?.countriesTableView?.tableView.reloadData()
            self?.countriesTableView?.progressBar.alpha = 0
        }, progress: { [weak self] progress in
            self?.countriesTableView?.progressBar.setProgress(Float(progress.progress), animated: true)
        })
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        countriesTableView?.progressBar.alpha = 1
        ServiceLayer.getAllCountry(completion: { [weak self] result in
            self?.tableController.models = result
            self?.countriesTableView?.tableView.reloadData()
            refreshControl.endRefreshing()
            self?.countriesTableView?.progressBar.alpha = 0
            }, progress: { [weak self] progress in
                self?.countriesTableView?.progressBar.setProgress(Float(progress.progress), animated: true)
        })
    }
    
    override func loadView() {
        let customView = CountriesTableView()
        customView.tableView.delegate = tableController
        customView.tableView.dataSource = tableController
        tableController.delegate = self
        customView.tableView.backgroundColor = CustomColor.lightSeparator
        view = customView
        self.countriesTableView = customView
    }
    
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CountriesController: CountriesTableControllerDelegate {
    func selectCountryViewController(_ controller: CountriesTableController, didSelect country: String) {
        let detailViewController = DetailViewController(nameCountry: country)
        let navigationController = UINavigationController(rootViewController: detailViewController)
        present(navigationController, animated: true, completion: nil)
    }
}
