//
//  CountriesTableView.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 26/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import UIKit

/// Flow позволяет кастомизировать Таблицу и элементы находящиеся на этой таблице
final class CountriesTableView: UIView {
    private var bottomTableContraint: NSLayoutConstraint!
    
    /// Таблица
    lazy var tableView: UITableView = {
        let view = UITableView(frame: .zero, style: .plain)
        view.separatorStyle = .none
        view.rowHeight = 80
        view.backgroundColor = UIColor.white
        view.allowsMultipleSelection = true
        return view
    }()
    
    /// Рефреш контроллер
    lazy var refreshControll: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = CustomColor.red
        return refreshControl
    }()
    
    // MARK: - Progress Bar
    lazy var progressBar: UIProgressView = {
        let progress = UIProgressView()
        progress.progressViewStyle = .bar
        progress.center = (tableView.center)
        progress.setProgress(0.5, animated: true)
        progress.trackTintColor = UIColor.black
        progress.tintColor = UIColor.red
        return progress
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let margins = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        tableView.frame = frame.inset(by: margins)
    }

    func updateBottomContraint(with newValue: CGFloat) {
        tableView.layoutIfNeeded()
    }
    
    init() {
        super.init(frame: .zero)
        addSubview(tableView)
        tableView.addSubview(refreshControll)
        tableView.addSubview(progressBar)
        makeConstraints()
    }
    
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CountriesTableView {
    private func makeConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        bottomTableContraint = tableView.heightAnchor.constraint(equalTo: heightAnchor)
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: leftAnchor),
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.widthAnchor.constraint(equalTo: widthAnchor),
            
            ])
    }
}

