//
//  CountriesTableViewCell.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 26/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import UIKit

class CountriesTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var populationLabel: UILabel!
}

extension CountriesTableViewCell {
    
    /// Конфигуратор ячейки
    ///
    /// - Parameter countries: Объект из массива
    func configure(_ countries: Countries) {
        nameLabel.text = countries.name
        populationLabel.text = String(countries.population)
    }
}
