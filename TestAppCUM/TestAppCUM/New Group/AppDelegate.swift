//
//  AppDelegate.swift
//  TestAppCUM
//
//  Created by Фамил Гаджиев on 26/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        /// Нужно вынести в кастомный НавКонтроллер (Времени не было, сорьки =( )
        let navigationViewController = UINavigationController(rootViewController: CountriesController())
        navigationViewController.navigationBar.prefersLargeTitles = true
        navigationViewController.navigationItem.largeTitleDisplayMode = .never
        navigationViewController.navigationBar.barTintColor = CustomColor.red
        
        UINavigationBar.appearance().largeTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor:    UIColor.white,
            NSAttributedString.Key.font:               UIFont.boldSystemFont(ofSize: 30)]
        
        window?.rootViewController = navigationViewController
        return true
    }
}

